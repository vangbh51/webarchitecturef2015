class HomeController < ApplicationController

  def index
  end

  def about
  end

  def contact
    render layout: false
  end

  def faq
  end

  def policy
  end

end
