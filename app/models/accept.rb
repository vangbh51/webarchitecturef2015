      class Accept < ActiveRecord::Base

  belongs_to :accepted, polymorphic: true
  belongs_to :acceptor, polymorphic: true

end
