class CreateAccepts < ActiveRecord::Migration
  def change
    create_table :accepts do |t|
      t.references :acceptor, polymorphic: true, index: true
      t.references :accepted, polymorphic: true, index: true

      t.timestamps null: false
    end
  end
end
