class CreateCategoriesChallenges < ActiveRecord::Migration
  def change
    create_table :categories_challenges, :id => false do |t|
      t.integer :challenge_id
      t.integer :category_id
    end
    add_index :categories_challenges, [:challenge_id, :category_id]
  end
end
