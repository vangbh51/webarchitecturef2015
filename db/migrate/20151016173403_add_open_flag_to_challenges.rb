class AddOpenFlagToChallenges < ActiveRecord::Migration
  def change
    add_column :challenges, :open_flag, :boolean, default: true
  end
end
