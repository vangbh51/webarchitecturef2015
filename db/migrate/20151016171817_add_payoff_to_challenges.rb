class AddPayoffToChallenges < ActiveRecord::Migration
  def change
    add_column :challenges, :payoff, :integer
  end
end
