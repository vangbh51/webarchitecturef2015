require "rails_helper"

RSpec.describe VoteChallengesController, type: :routing do
  describe "routing" do

    it "routes to #index" do
      expect(:get => "/vote_challenges").to route_to("vote_challenges#index")
    end

    it "routes to #new" do
      expect(:get => "/vote_challenges/new").to route_to("vote_challenges#new")
    end

    it "routes to #show" do
      expect(:get => "/vote_challenges/1").to route_to("vote_challenges#show", :id => "1")
    end

    it "routes to #edit" do
      expect(:get => "/vote_challenges/1/edit").to route_to("vote_challenges#edit", :id => "1")
    end

    it "routes to #create" do
      expect(:post => "/vote_challenges").to route_to("vote_challenges#create")
    end

    it "routes to #update via PUT" do
      expect(:put => "/vote_challenges/1").to route_to("vote_challenges#update", :id => "1")
    end

    it "routes to #update via PATCH" do
      expect(:patch => "/vote_challenges/1").to route_to("vote_challenges#update", :id => "1")
    end

    it "routes to #destroy" do
      expect(:delete => "/vote_challenges/1").to route_to("vote_challenges#destroy", :id => "1")
    end

  end
end
