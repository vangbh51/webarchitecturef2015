require 'rails_helper'

RSpec.describe "vote_challenges/show", type: :view do
  before(:each) do
    @vote_challenge = assign(:vote_challenge, VoteChallenge.create!(
      :user_id => 1,
      :challenge_id => 2
    ))
  end

  it "renders attributes in <p>" do
    render
    expect(rendered).to match(/1/)
    expect(rendered).to match(/2/)
  end
end
